import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Chats from '@/components/Chats'
import Login from '@/components/Login'
import Register from '@/components/Register'
import PageNotFound from '@/components/PageNotFound'
import Profile from '@/components/Profile'
import Concerts from '@/components/Concerts'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      meta: { requiresAuth: true },
      component: HelloWorld
    },
    {
      path: '/profile/:username?',
      meta: { requiresAuth: true },
      name: 'Profile',
      component: Profile
    },
    {
      path: '/chats',
      name: 'Chats',
      meta: { requiresAuth: true },
      component: Chats
    },
    {
      path: '/concerts',
      name: 'Concerts',
      meta: { requiresAuth: true },
      component: Concerts
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/pageNotFound',
      name: 'PageNotFound',
      component: PageNotFound
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (!to.matched.length || to.matched.length==0) {
    next({
      path: '/pageNotFound',
      query: { redirect: to.fullPath }
    })
  }else {
    next();
  }
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!localStorage.getItem('token')) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }

})

router.onError(err=>{
  console.log(err)
})

export default router;
