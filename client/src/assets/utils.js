export function getInputFile(event){
  if(event.target.files[0]){
    var file = {
      blob:null,
      input:null
    }
    file.input = event.target.files[0]

    var reader = new FileReader();
              reader.onload = (event) => {
                  file.blob = event.target.result;
              };
              reader.readAsDataURL(file.input);              
              return file
  }else {
    return false;
  }

}
