import axios from 'axios'

var apiRoute = 'http://localhost:3000';

function onFullfilled(response) {
    return response;
}
function onRejected(err) {
    return Promise.reject(err);
}

export default {
  chats () {
    return axios.create({
      baseURL: apiRoute+`/chats`
    })
  },

  auth () {
    var apiAuth = axios.create({
      baseURL: apiRoute+`/users`
    })
    apiAuth.interceptors.response.use(onFullfilled, onRejected)
    return apiAuth
  },

  posts () {
    var apiAuth = axios.create({
      baseURL: apiRoute+`/posts`
    })
    apiAuth.interceptors.response.use(onFullfilled, onRejected)
    return apiAuth
  },
  concerts () {
    var apiAuth = axios.create({
      baseURL: apiRoute+`/concerts`
    })
    apiAuth.interceptors.response.use(onFullfilled, onRejected)
    return apiAuth
  }

}
