import Api from '@/api/Api'
import router from '@/router'


// initial state
const state = {
  profile: null
}

// getters
const getters = {
  profile: state => state.profile,
}

// actions
const actions = {

getUserByUsername({commit},usrnm) {
  return Api.auth().post('/getUserByUsername',{username:usrnm}).then((res) =>
    {
        commit('setProfile', res.data.user)
    }).catch((res) => {
      return res
    })
},

updateUser({commit},user) {
  return Api.auth().put('/update',user).then((res) =>
    {
        commit('setProfile', res.data.user)
    }).catch((res) => {
      return res
    })
}

}

// mutations
const mutations = {
  setProfile (state, profile) {
    state.profile = profile
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
