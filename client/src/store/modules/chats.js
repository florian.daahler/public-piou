import Api from '@/api/Api'

// initial state
const state = {
  all: []
}

// getters
const getters = {
  allChats: state => state.all
}

// actions
const actions = {
  getAllChats ({ commit }) {
    return Api.chats().get('/get_chats').then((res) =>
      {
        commit('setChats', res.data.chats)
      })
  },

  resetChat ({ commit ,state},rstChats) {
    commit('setChats', rstChats)
  }
}

// mutations
const mutations = {
  setChats (state, chats) {
    state.all = chats
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
