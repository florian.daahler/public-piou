import Api from '@/api/Api'
import router from '@/router'
const namespaced = true;
// initial state
const state = {
  authedProfile: {},
  isLoggedIn: false,
  playingSong: null
}

// getters
const getters = {

}

// actions
const actions = {

authProfile({commit},credentials) {
  return Api.auth().post('/authenticate',{username:credentials.username,password:credentials.password}).then((res) =>
    {
      if (res.data.success){
        localStorage.setItem('token',res.data.token);
        commit('setIsLoggedIn',true);
        commit('setAuthedProfile', res.data.user)
        router.push({ path: '/' })
      }else {
        commit('setAuthedProfile',false)
      }
    }).catch((res) => {
      return res
    })
},
getAuthedProfile({commit}){
  if(state.isLoggedIn){
    return Api.auth().get('/profile',{ headers: { Authorization: localStorage.getItem('token')} }).then((res) => {
      commit('setAuthedProfile', res.data.user)
    }).catch((res) => {
      if(!!res){
        localStorage.removeItem('token');
        router.push({ path: '/login' })
      }
    })
  }else {
    return false;
  }
},

getIsLoggedIn({commit}){
  commit('setIsLoggedIn',!!localStorage.getItem("token"))
},
togglePlayingSong({commit},playingSong){
  var isAnewSong = !this.state.profile.playingSong || playingSong.name!=this.state.profile.playingSong.name || playingSong.artist!=this.state.profile.playingSong.artist;
  commit('setPlayingSong',{...playingSong,...{isPlaying:!playingSong.isPlaying}})
  var audioDom = (document.getElementById('arb-player'))
  if (isAnewSong) {
    audioDom.load();
  }
  if(!playingSong.isPlaying) {
    audioDom.play()
  }
  else {
    audioDom.pause()
  }
},
registerProfile({commit},credentials){
  return Api.auth().post('/register',credentials).then((res) =>
    {      
      if (res.data.success){
        router.push({ path: '/login' })
      }else {
        return false;
      }
    }).catch((res) => {
      return res
    })
}
}

// mutations
const mutations = {
  setAuthedProfile (state, profile) {
    state.authedProfile = { ...profile }
  },
  addNewPost(state,newPost) {
    state.authedProfile.posts.push(newPost)
  },
  setIsLoggedIn (state, isLoggedIn) {
    state.isLoggedIn = isLoggedIn
  },
  setPlayingSong (state, playingSong) {
    state.playingSong = { ...playingSong }
  }
}

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
