const mongoose = require('mongoose');
const config = require('../config/database');
const User = require('../models/user');

//Post Schema
const PostSchema = mongoose.Schema({
    creator:{
        type: String,
        required:true
    },
    url: {
        type:String,
        required:true
    },
    artist: {
        type:String
    },
    name: {
          type:String          
      },
      description: {
          type:String,

      },
      tags: {
          type:Array
      },
      gender:{
        type:String
      },
      date:{
        type:String,
        required:true
      }
});

const Post = module.exports = mongoose.model('Post', PostSchema);

module.exports.addPost = function(newPost, callback){
    User.
  findOne({ _id: newPost.creator },function(err,user){
    if (err) {
			console.log(err);
			return ;
		}
    user.posts.push(newPost);
    user.save(function(err2) {
			if (err2) {
				console.log(err2);
			}

			return callback();
		});
  });
}
