const mongoose = require('mongoose');
const config = require('../config/database');

//Post Schema
const ConcertSchema = mongoose.Schema({
    artist:{
        type: String,
    },
    exposant:{
        type: String,
    },
    date_created:{
        type: String,
    },
    step: {
        type:Number,
    },
    messages: {
        type:Array,
    },
    informations:{
      type:Object
    }
});

const Concerts = module.exports = mongoose.model('Concerts', ConcertSchema);

module.exports.addConcert = function(newConcert, callback){
  newConcert.save(callback);
}

module.exports.getConcert = (concert,callback) => {
  Concerts.find({ $or: [ { artist: concert.authed,exposant: concert.profile }, { artist: concert.profile,exposant: concert.authed } ] },callback);
}
