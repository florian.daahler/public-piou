const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

//User Schema
const UserSchema = mongoose.Schema({
    name:{
        type: String
    },
    email: {
        type:String,
        required:true
    },
    username: {
        type:String,
        required:true
    },
    informations: {
      type:Object,
      required: false
    },
    type: {
        type:String,
        required:true
    },
    password: {
        type:String,
        required:true
    },
    profilePicture: {
      type: Object,
      required:false
    },
    posts: {type:Array},
    followings: {type:Array}
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function(id, callback){
    User.findById(id, callback);
}

module.exports.getUserByUsername = function(username, callback){
    const query = {username: username}
    User.findOne(query, callback);
}

module.exports.updateUser = function(email,user,callback){
  let query = {email: email};
  let update = {$set:user};
  User.findOneAndUpdate(query, update, {new: true}, callback);
}

module.exports.addUser = function(newUser, callback){
    bcrypt.genSalt(10,(err,salt)=>{
        bcrypt.hash(newUser.password, salt, (err,hash)=>{
            if(err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
    bcrypt.compare(candidatePassword, hash, (err, isMatch)=>{
        if(err) throw err;
        callback(null, isMatch);
    });
}
