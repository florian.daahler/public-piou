const mongoose = require('mongoose');
const config = require('../config/database');

//Post Schema
const ChatSchema = mongoose.Schema({
    from:{
        type: String,
    },
    to:{
        type: String,
    },
    date_created:{
        type: String,
    },
    messages: {
        type:Array,
    },
    unread: {
        type:Boolean,
    }
});

const Chat = module.exports = mongoose.model('Chat', ChatSchema);

module.exports.getChats = (req,res) => {
  Chat.find().exec((err,chats) => {
    if(err){
    return res.json({'success':false,'message':'Some Error'});
    }
return res.json({'success':true,'message':'Chats fetched successfully','chats':chats});
  });
}

module.exports.getChatsByUsername = (req,res) => {
  Chat.find({$or:[
        {from: req.body.username},
        {to: req.body.username}
    ]}).exec((err,chats) => {
    if(err){
    return res.json({'success':false,'message':'Some Error'});
    }
return res.json({'success':true,'message':'Chats fetched successfully','chats':chats});
  });
}

module.exports.addChat = (io,T) => {
  let result;
  const newChat = new Chat(T);
  newChat.save((err,chat) => {
    if(err){
      result = {'success':false,'message':'Some Error','error':err};
      console.log(result);
    }
    else{
      const result = {'success':true,'message':'Chat Added Successfully',chat}
       io.in(T.from).emit('chatAdded', result);
       io.in(T.to).emit('chatAdded', result);
    }
  })
}

module.exports.sendMessage = (io,T) => {
  let result;
  let query = { _id:T._id };
  let update = {$set:T};
  Chat.findOneAndUpdate(query, update, { new:true }, (err,chat) => {
    if(err){
    result = {'success':false,'message':'Some Error','error':err};
    console.log(result);
    }
    else{
     result = {'success':true,'message':'message sent successfully',chat};
     console.log(chat);
     io.in(T.from).emit('messageSent', result);
     io.in(T.to).emit('messageSent', result);
    }
  })
}

module.exports.updateChatState = (req,res) => {
  let result;  
  let query = { _id:req.body._id };
  let update = {$set:req.body};
  Chat.findOneAndUpdate(query, update, { new:true }, (err,chat) => {
    if(err){
    result = {'success':false,'message':'Some Error','error':err};
    console.log(result);
    }
    else{
     result = {'success':true,'message':'message state successfully updated',chat};
    }
  })
}
