const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');


// Connect To Database
mongoose.connect(config.database, { useMongoClient: true });

// On Connection
mongoose.connection.on('connected', () => {
  // console.log('Connected to database '+config.database);
  console.log('Connected to Mongo')
});

// On Error
mongoose.connection.on('error', (err) => {
  console.log('Database error: '+err);
});

const app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

const users = require('./routes/users');
const posts = require('./routes/posts');
const chats = require('./routes/chats');
const concerts = require('./routes/concerts');
const Chat = require('./models/chat');

// Port Number
const port = 3000;

// CORS Middleware
app.use(cors());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.json());

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users', users);
app.use('/posts', posts);
app.use('/chats', chats);
app.use('/concerts', concerts);

// Index Route
app.get('/', (req, res) => {
  res.send('Invalid Endpoint');
});

//listen to socket
io.on('connection', function(socket){
  console.log("Connected to Socket!!"+ socket.id);

socket.on('room', function(room) {
  console.log(room);
          socket.join(room);
      });

// Receiving chats from client
socket.on('addChat', (message) => {
  console.log('socketData: '+JSON.stringify(message));
  Chat.addChat(io,message);
});
// Receiving Updated chat from client
  socket.on('sendMessage', (chat) => {
    console.log('socketData: '+JSON.stringify(chat));
    Chat.sendMessage(io,chat);
  });
});


app.get('*', ()=>{
    res.sendFile(path.join(__dirname, 'public/index.tml'));
});
// Start Server
http.listen(port, function(){
  console.log('listening on *:3000');
});
