
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const User = require('../models/user');
var multer = require('multer');

// Register
router.post('/register', (req, res, next) => {
  let newUser = new User({
    name: req.body.name,
    email: req.body.email,
    type: req.body.type,
    username: req.body.username,
    password: req.body.password,
    profilePicture:{url:""},
    informations:{description:""}
  });
//Add user
  User.addUser(newUser, (err, user) => {
    if(err){
      res.json({success: false, msg:'Failed to register user'});
    } else {
      res.json({success: true, msg:'User registered'});
    }
  });
});

//Update User
router.put("/update", (req, res) => {
    let user = req.body;
    let userEmail = req.body.email;
    User.updateUser(userEmail,user, (err)=>{
      if(err){
        return res.json({success: false, msg: 'Something went wrong with upload'});
      }
      else {
        return res.json({success:true, msg: 'Profile updated'});
      }
    });

});

//Upload profil picture
var storage = multer.diskStorage({
  // destination
  destination: function (req, file, cb) {
    cb(null, './public/profile-pictures')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});
var upload = multer({ storage: storage });

router.post("/upload_profile_picture", upload.array("uploads[]", 12), function (req, res) {
  res.send(req.files);
});


// Authenticate
router.post('/authenticate', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  User.getUserByUsername(username, (err, user) => {
    if(err) throw err;
    if(!user){
      return res.json({success: false, msg: 'User not found'});
    }

    User.comparePassword(password, user.password, (err, isMatch) => {
      if(err) throw err;
      if(isMatch){
        const token = jwt.sign({data: user}, config.secret, {
          expiresIn: 604800 // 1 week
        });

        res.json({
          success: true,
          token: 'JWT '+token,
          user: {
            email:user.email,
            type:user.type,
            name:user.name,
            posts:user.posts,
            followings:user.followings,
            profilePicture:user.profilePicture,
            username:user.username,
            informations:user.informations,
            _id:user._id
          }
        });
      } else {
        return res.json({success: false, msg: 'Wrong password'});
      }
    });
  });
});

// Profile
router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  res.json({user: {
    email:req.user.email,
    type:req.user.type,
    name:req.user.name,
    posts:req.user.posts,
    followings:req.user.followings,
    profilePicture:req.user.profilePicture,
    username:req.user.username,
    informations:req.user.informations,
    _id:req.user._id
  }});
});

router.post('/profile_by_username',(req, res, next) => {
  const username = req.body.username;
  User.getUserByUsername(username, (err, user) => {
    if(err){      
      return false;
    } if (!user) {
      res.json({
        success:false,
        msg:"Il semblerait que cet utilisateur n'existe pas"
      });
    }else{
      res.json({
        success:true,
        msg:"",
        user: {
          email:user.email,
          name:user.name,
          type:user.type,
          posts:user.posts,
          followings:user.followings,
          profilePicture:user.profilePicture,
          username:user.username,
          informations:user.informations,
          _id:user._id
        }
      });
    }
  });
});

module.exports = router;
