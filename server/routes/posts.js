
const express = require('express');
const router = express.Router();
const Post = require('../models/post');
var multer = require('multer');


//Upload music post
var musicStorage = multer.diskStorage({
  // destination
  destination: function (req, file, cb) {
    cb(null, './public/profile-posts/songs')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});
var musicUpload = multer({ storage: musicStorage });

router.post("/upload_song_post", musicUpload.array("uploads[]", 12), function (req, res) {
  res.send(req.files);
});

//Upload image post
var imgStorage = multer.diskStorage({
  // destination
  destination: function (req, file, cb) {
    cb(null, './public/profile-posts/imgs')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});
var imgUpload = multer({ storage: imgStorage });

router.post("/upload_img_post", imgUpload.array("uploads[]", 12), function (req, res) {
  res.send(req.files);
});

// Add Post
router.post('/add_post', (req, res, next) => {
  let newPost = new Post({
    creator: req.body.creator,
    url: req.body.url,
    artist:req.body.artist,
    name:req.body.name,
    description:req.body.description,
    tags:req.body.tags,
    date:req.body.date,
    gender:req.body.gender
  });

  Post.addPost(newPost, (err, user) => {
    if(err){
      res.json({success: false, msg:'Failed to register Post'});
    } else {
      res.json({success: true, msg:'Post registered'});
    }
  });
});

module.exports = router;
