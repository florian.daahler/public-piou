const express = require('express');
const router = express.Router();
const Chat = require('./../models/chat');


router.get('/get_chats', (req, res, next) => {
  Chat.getChats(req,res)
});

router.post('/get_chats_by_username', (req, res, next) => {
  Chat.getChatsByUsername(req,res);
});

router.post('/update_chat_state', (req, res, next) => {
  Chat.updateChatState(req,res);
});


module.exports = router;
