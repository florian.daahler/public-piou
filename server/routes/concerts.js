const express = require('express');
const router = express.Router();
const Concerts = require('./../models/concert');

router.post('/create_concert', (req, res, next) => {
  let newConcert = new Concerts({
    artist: req.body.artist,
    exposant: req.body.exposant,
    date_created: req.body.date_created,
    step: req.body.step,
    messages: [],
    informations: {}
  });
//Add user
  Concerts.addConcert(newConcert, (err, concert) => {
    if(err){
      res.json({success: false, msg:'Failed to create concert'});
    } else {
      res.json({success: true, msg:'Concert created'});
    }
  });


});

router.post('/get_concert', (req, res, next) => {
  let concert = {
    authed: req.body.authed,
    profile:req.body.profile
  };
  Concerts.getConcert(concert, (err, concert) => {
    if(err){
      res.json({success: false, msg:'cannot find concert',error:err});
    } else {
      res.json({success: true, msg:'Concert finded',concert:concert});
    }
  });


});


module.exports = router;
